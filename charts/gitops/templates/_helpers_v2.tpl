{{- define "skimia.gitops.v2.app.valuesFiles" -}}
{{- $top := first . -}}
{{- $onlyApp := eq (typeOf (last .))  "bool" | ternary (last .) false -}}
{{- $sk := $top.Values.gitops -}}
{{- $appFile := (index . 1) }}
{{- if not $onlyApp }}
- $values/default/main.yaml
{{- end }}
- $values/default/{{$appFile}}.yaml
{{- if not $onlyApp }}
- $values/{{$sk.env}}/all/main.yaml
{{- end }}
- $values/{{$sk.env}}/all/{{$appFile}}.yaml
{{- if not $onlyApp }}
- $values/{{$sk.env}}/{{$sk.cluster}}/main.yaml
{{- end }}
- $values/{{$sk.env}}/{{$sk.cluster}}/{{$appFile}}.yaml
{{- end -}}

{{- define "skimia.gitops.v2.app.syncPolicy" }}
{{- $top := first . -}}
{{- $sk := $top.Values.gitops -}}
{{- $app := deepCopy (index . 1) | mergeOverwrite $sk.app  }}
syncPolicy:
{{- if $app.syncPolicy.automated.enabled }}
{{- if and (not $app.syncPolicy.automated.prune) (not $app.syncPolicy.automated.selfHeal) (not $app.syncPolicy.automated.allowEmpty) }}
  automated: {}
{{ else }}
  automated:
    prune: {{ $app.syncPolicy.automated.prune | ternary "true" "false" }}
    selfHeal: {{ $app.syncPolicy.automated.selfHeal | ternary "true" "false" }}
    allowEmpty: {{ $app.syncPolicy.automated.allowEmpty | ternary "true" "false" }}
{{- end }}
{{- end }}
  syncOptions:
  {{- range $key, $value := $app.syncPolicy.syncOptions }}
  - {{title $key}}={{$value}}
  {{- end }}
{{- end -}}


{{- define "skimia.gitops.v2.name" }}
{{- $top := first . -}}
{{- $name := last . -}}
{{- $sk := $top.Values.gitops -}}
{{- if $sk.clusterPrefix -}}
{{ $sk.env }}-{{ $sk.cluster }}-{{ $name }}
{{- else -}}
{{ $name }}
{{- end -}}
{{- end -}}
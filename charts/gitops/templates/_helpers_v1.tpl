{{- define "skimia.gitops.v1.app.valuesRepoSource" }}
- repoURL: {{ .Values.gitops.valuesRepo }}
  targetRevision: HEAD
  ref: values
{{- end -}}

{{- define "skimia.gitops.v1.app.info" }}
info:
  - name: 'Example:'
    value: 'https://example.com'
{{- end -}}

---
apiVersion: cluster.x-k8s.io/v1beta1
kind: Cluster
metadata:
  labels:
    cluster.x-k8s.io/cluster-name: {{ include "cluster.fullname" . }}
    cluster.x-k8s.io/cni: {{ .Values.interfaces.cni }}
    cluster.x-k8s.io/csi: {{ .Values.interfaces.csi }}
    cluster.x-k8s.io/cpi: {{ .Values.interfaces.cpi }}
  name: {{ include "cluster.fullname" . }}
  namespace: {{ .Release.Namespace }}
spec:
  clusterNetwork:
    pods:
      cidrBlocks:
      {{- toYaml .Values.podsCidrBlocks | nindent 6 }}
  controlPlaneRef:
    apiVersion: controlplane.cluster.x-k8s.io/v1beta1
    kind: KubeadmControlPlane
    name: {{ include "cluster.fullname" . }}
  infrastructureRef:
    apiVersion: infrastructure.cluster.x-k8s.io/v1beta1
    kind: VSphereCluster
    name: {{ include "cluster.fullname" . }}
---
apiVersion: infrastructure.cluster.x-k8s.io/v1beta1
kind: VSphereCluster
metadata:
  name: {{ include "cluster.fullname" . }}
  namespace: {{ .Release.Namespace }}
spec:
  controlPlaneEndpoint:
    host: {{ .Values.controlPlane.endpoint.vip }}
    port: {{ .Values.controlPlane.endpoint.port }}
  identityRef:
    kind: Secret
    name: {{ include "cluster.fullname" . }}
  server: {{ .Values.vsphere.host }}
  thumbprint: {{ .Values.vsphere.thumbprint }}
---
apiVersion: infrastructure.cluster.x-k8s.io/v1beta1
kind: VSphereMachineTemplate
metadata:
  name: {{ include "cluster.fullname" . }}
  namespace: {{ .Release.Namespace }}
spec:
  template:
    spec:
      cloneMode: {{ coalesce .Values.controlPlane.cloneMode .Values.defaults.cloneMode }}
      datacenter: {{ coalesce .Values.controlPlane.datacenter .Values.defaults.datacenter (first .Values.vsphere.datacenters) }}
      datastore: {{ coalesce .Values.controlPlane.datastore .Values.defaults.datastore }}
      diskGiB: {{ coalesce .Values.controlPlane.diskGiB .Values.defaults.diskGiB }}
      folder: {{ coalesce .Values.controlPlane.VMfolder .Values.defaults.VMfolder }}
      memoryMiB: {{ coalesce .Values.controlPlane.memoryMiB .Values.defaults.memoryMiB }}
      network:
        devices:
        {{- coalesce .Values.controlPlane.networks .Values.defaults.networks | toYaml | nindent 8 }}
      numCPUs: {{ coalesce .Values.controlPlane.numCPUs .Values.defaults.numCPUs }}
      os: {{ coalesce .Values.controlPlane.os .Values.defaults.os }}
      powerOffMode: {{ coalesce .Values.controlPlane.powerOffMode .Values.defaults.powerOffMode }}
      resourcePool: '{{ coalesce .Values.controlPlane.resourcePool .Values.defaults.resourcePool}}'
      server: {{ .Values.vsphere.host }}
      storagePolicyName: {{ coalesce .Values.controlPlane.storagePolicyName .Values.defaults.storagePolicyName}}
      template: {{ coalesce .Values.controlPlane.template .Values.defaults.template}}
      thumbprint: {{ .Values.vsphere.thumbprint }}
---
apiVersion: infrastructure.cluster.x-k8s.io/v1beta1
kind: VSphereMachineTemplate
metadata:
  name: {{ include "cluster.fullname" . }}-worker
  namespace: {{ .Release.Namespace }}
spec:
  template:
    spec:
      cloneMode: {{ coalesce .Values.worker.cloneMode .Values.defaults.cloneMode }}
      datacenter: {{ coalesce .Values.worker.datacenter .Values.defaults.datacenter (first .Values.vsphere.datacenters) }}
      datastore: {{ coalesce .Values.worker.datastore .Values.defaults.datastore }}
      diskGiB: {{ coalesce .Values.worker.diskGiB .Values.defaults.diskGiB }}
      folder: {{ coalesce .Values.worker.VMfolder .Values.defaults.VMfolder }}
      memoryMiB: {{ coalesce .Values.worker.memoryMiB .Values.defaults.memoryMiB }}
      network:
        devices:
        {{- coalesce .Values.worker.networks .Values.defaults.networks | toYaml | nindent 8 }}
      numCPUs: {{ coalesce .Values.worker.numCPUs .Values.defaults.numCPUs }}
      os: {{ coalesce .Values.worker.os .Values.defaults.os }}
      powerOffMode: {{ coalesce .Values.worker.powerOffMode .Values.defaults.powerOffMode }}
      resourcePool: '{{ coalesce .Values.worker.resourcePool .Values.defaults.resourcePool}}'
      server: {{ .Values.vsphere.host }}
      storagePolicyName: {{ coalesce .Values.worker.storagePolicyName .Values.defaults.storagePolicyName}}
      template: {{ coalesce .Values.worker.template .Values.defaults.template}}
      thumbprint: {{ .Values.vsphere.thumbprint }}
---
apiVersion: controlplane.cluster.x-k8s.io/v1beta1
kind: KubeadmControlPlane
metadata:
  name: {{ include "cluster.fullname" . }}
  namespace: {{ .Release.Namespace }}
spec:
  kubeadmConfigSpec:
    clusterConfiguration:
      apiServer:
        extraArgs:
          cloud-provider: external
      controllerManager:
        extraArgs:
          cloud-provider: external
{{ if .Values.controlPlane.endpoint.kubeVip.enable }}
    files:
    - content: |
        apiVersion: v1
        kind: Pod
        metadata:
          creationTimestamp: null
          name: kube-vip
          namespace: kube-system
        spec:
          containers:
          - args:
            - manager
            env:
            - name: cp_enable
              value: "true"
            - name: vip_interface
              value: ""
            - name: address
              value: {{ .Values.controlPlane.endpoint.vip }}
            - name: port
              value: "{{ .Values.controlPlane.endpoint.port }}"
            - name: vip_arp
              value: "true"
            - name: vip_leaderelection
              value: "true"
            - name: vip_leaseduration
              value: "15"
            - name: vip_renewdeadline
              value: "10"
            - name: vip_retryperiod
              value: "2"
            # Added from official
            - name: enableUPNP
              value: "{{ .Values.controlPlane.endpoint.kubeVip.enableUPNP | ternary "true" "false" }}"
            - name: svc_enable
              value: "{{ .Values.controlPlane.endpoint.kubeVip.svcEnable | ternary "true" "false" }}"
            # End Added from official
            image: ghcr.io/kube-vip/kube-vip:v0.5.11
            imagePullPolicy: IfNotPresent
            name: kube-vip
            resources: {}
            securityContext:
              capabilities:
                add:
                - NET_ADMIN
                - NET_RAW
            volumeMounts:
            - mountPath: /etc/kubernetes/admin.conf
              name: kubeconfig
          hostAliases:
          - hostnames:
            - kubernetes
            ip: 127.0.0.1
          hostNetwork: true
          volumes:
          - hostPath:
              path: /etc/kubernetes/admin.conf
              type: FileOrCreate
            name: kubeconfig
        status: {}
      owner: root:root
      path: /etc/kubernetes/manifests/kube-vip.yaml
{{ end }}
    initConfiguration:
      nodeRegistration:
        criSocket: /var/run/containerd/containerd.sock
        kubeletExtraArgs:
          cloud-provider: external
        name: '{{ "{{ local_hostname }}" }}'
    joinConfiguration:
      nodeRegistration:
        criSocket: /var/run/containerd/containerd.sock
        kubeletExtraArgs:
          cloud-provider: external
        name: '{{ "{{ ds.meta_data.hostname }}" }}'
    preKubeadmCommands:
    {{`- hostnamectl set-hostname "{{ ds.meta_data.hostname }}"
    - echo "::1         ipv6-localhost ipv6-loopback localhost6 localhost6.localdomain6"
      >/etc/hosts
    - echo "127.0.0.1   {{ ds.meta_data.hostname }} {{ local_hostname }} localhost
      localhost.localdomain localhost4 localhost4.localdomain4" >>/etc/hosts`}}
    users:
    {{- .Values.sshUsers | toYaml | nindent 4 }}
  machineTemplate:
    infrastructureRef:
      apiVersion: infrastructure.cluster.x-k8s.io/v1beta1
      kind: VSphereMachineTemplate
      name: {{ include "cluster.fullname" . }}
  replicas: {{ .Values.controlPlane.replicas }}
  version: {{ .Values.kube.version }}
  rolloutBefore:
    certificatesExpiryDays: 21
---
apiVersion: bootstrap.cluster.x-k8s.io/v1beta1
kind: KubeadmConfigTemplate
metadata:
  name: {{ include "cluster.fullname" . }}-md-0
  namespace: {{ .Release.Namespace }}
spec:
  template:
    spec:
      joinConfiguration:
        nodeRegistration:
          criSocket: /var/run/containerd/containerd.sock
          kubeletExtraArgs:
            cloud-provider: external
          name: '{{ "{{ local_hostname }}" }}'
      preKubeadmCommands:
      {{`- hostnamectl set-hostname "{{ ds.meta_data.hostname }}"
      - echo "::1         ipv6-localhost ipv6-loopback localhost6 localhost6.localdomain6"
        >/etc/hosts
      - echo "127.0.0.1   {{ ds.meta_data.hostname }} {{ local_hostname }} localhost
        localhost.localdomain localhost4 localhost4.localdomain4" >>/etc/hosts`}}
      users:
      {{- .Values.sshUsers | toYaml | nindent 6 }}
---
apiVersion: cluster.x-k8s.io/v1beta1
kind: MachineDeployment
metadata:
  labels:
    cluster.x-k8s.io/cluster-name: {{ include "cluster.fullname" . }}
  name: {{ include "cluster.fullname" . }}-md-0
  namespace: {{ .Release.Namespace }}
spec:
  clusterName: {{ include "cluster.fullname" . }}
  replicas: {{ .Values.worker.replicas }}
  selector:
    matchLabels: {}
  template:
    metadata:
      labels:
        cluster.x-k8s.io/cluster-name: {{ include "cluster.fullname" . }}
    spec:
      bootstrap:
        configRef:
          apiVersion: bootstrap.cluster.x-k8s.io/v1beta1
          kind: KubeadmConfigTemplate
          name: {{ include "cluster.fullname" . }}-md-0
      clusterName: {{ include "cluster.fullname" . }}
      infrastructureRef:
        apiVersion: infrastructure.cluster.x-k8s.io/v1beta1
        kind: VSphereMachineTemplate
        name: {{ include "cluster.fullname" . }}-worker
      version: {{ .Values.kube.version }}
---
apiVersion: addons.cluster.x-k8s.io/v1beta1
kind: ClusterResourceSet
metadata:
  labels:
    cluster.x-k8s.io/cluster-name: {{ include "cluster.fullname" . }}
  name: {{ include "cluster.fullname" . }}-crs-0
  namespace: {{ .Release.Namespace }}
spec:
  clusterSelector:
    matchLabels:
      cluster.x-k8s.io/cluster-name: {{ include "cluster.fullname" . }}
  resources:
  - kind: Secret
    name: {{ include "cluster.fullname" . }}-vsphere-csi-controller
  - kind: ConfigMap
    name: {{ include "cluster.fullname" . }}-vsphere-csi-controller-role
  - kind: ConfigMap
    name: {{ include "cluster.fullname" . }}-vsphere-csi-controller-binding
  - kind: Secret
    name: {{ include "cluster.fullname" . }}-csi-vsphere-config
  - kind: ConfigMap
    name: {{ include "cluster.fullname" . }}-csi.vsphere.vmware.com
  - kind: ConfigMap
    name: {{ include "cluster.fullname" . }}-vsphere-csi-node
  - kind: ConfigMap
    name: {{ include "cluster.fullname" . }}-vsphere-csi-controller
  - kind: Secret
    name: {{ include "cluster.fullname" . }}-cloud-controller-manager
  - kind: Secret
    name: {{ include "cluster.fullname" . }}-cloud-provider-vsphere-credentials
  - kind: ConfigMap
    name: {{ include "cluster.fullname" . }}-cpi-manifests
---
apiVersion: v1
kind: Secret
metadata:
  name: {{ include "cluster.fullname" . }}
  namespace: {{ .Release.Namespace }}
stringData:
  password: {{ .Values.vsphere.password }}
  username: {{ .Values.vsphere.username }}
---
apiVersion: v1
kind: Secret
metadata:
  name: {{ include "cluster.fullname" . }}-vsphere-csi-controller
  namespace: {{ .Release.Namespace }}
stringData:
  data: |
    apiVersion: v1
    kind: ServiceAccount
    metadata:
      name: vsphere-csi-controller
      namespace: kube-system
type: addons.cluster.x-k8s.io/resource-set
---
apiVersion: v1
data:
  data: |
    apiVersion: rbac.authorization.k8s.io/v1
    kind: ClusterRole
    metadata:
      name: vsphere-csi-controller-role
    rules:
    - apiGroups:
      - storage.k8s.io
      resources:
      - csidrivers
      verbs:
      - create
      - delete
    - apiGroups:
      - ""
      resources:
      - nodes
      - pods
      - secrets
      - configmaps
      verbs:
      - get
      - list
      - watch
    - apiGroups:
      - ""
      resources:
      - persistentvolumes
      verbs:
      - get
      - list
      - watch
      - update
      - create
      - delete
      - patch
    - apiGroups:
      - storage.k8s.io
      resources:
      - volumeattachments
      verbs:
      - get
      - list
      - watch
      - update
      - patch
    - apiGroups:
      - storage.k8s.io
      resources:
      - volumeattachments/status
      verbs:
      - patch
    - apiGroups:
      - ""
      resources:
      - persistentvolumeclaims
      verbs:
      - get
      - list
      - watch
      - update
    - apiGroups:
      - storage.k8s.io
      resources:
      - storageclasses
      - csinodes
      verbs:
      - get
      - list
      - watch
    - apiGroups:
      - ""
      resources:
      - events
      verbs:
      - list
      - watch
      - create
      - update
      - patch
    - apiGroups:
      - coordination.k8s.io
      resources:
      - leases
      verbs:
      - get
      - watch
      - list
      - delete
      - update
      - create
    - apiGroups:
      - snapshot.storage.k8s.io
      resources:
      - volumesnapshots
      verbs:
      - get
      - list
    - apiGroups:
      - snapshot.storage.k8s.io
      resources:
      - volumesnapshotcontents
      verbs:
      - get
      - list
kind: ConfigMap
metadata:
  name: {{ include "cluster.fullname" . }}-vsphere-csi-controller-role
  namespace: {{ .Release.Namespace }}
---
apiVersion: v1
data:
  data: |
    apiVersion: rbac.authorization.k8s.io/v1
    kind: ClusterRoleBinding
    metadata:
      name: vsphere-csi-controller-binding
    roleRef:
      apiGroup: rbac.authorization.k8s.io
      kind: ClusterRole
      name: vsphere-csi-controller-role
    subjects:
    - kind: ServiceAccount
      name: vsphere-csi-controller
      namespace: kube-system
kind: ConfigMap
metadata:
  name: {{ include "cluster.fullname" . }}-vsphere-csi-controller-binding
  namespace: {{ .Release.Namespace }}
---
apiVersion: v1
kind: Secret
metadata:
  name: {{ include "cluster.fullname" . }}-csi-vsphere-config
  namespace: {{ .Release.Namespace }}
stringData:
  data: |
    apiVersion: v1
    kind: Secret
    metadata:
      name: csi-vsphere-config
      namespace: kube-system
    stringData:
      csi-vsphere.conf: |+
        [Global]
        thumbprint = "{{ .Values.vsphere.thumbprint }}"
        cluster-id = "default/{{ include "cluster.fullname" . }}"

        [VirtualCenter "{{ .Values.vsphere.host }}"]
        insecure-flag = "{{.Values.vsphere.insecure}}"
        user = "{{ .Values.vsphere.username }}"
        password = "{{ .Values.vsphere.password }}"
        datacenters = "{{ join "," .Values.vsphere.datacenters }}"

        [Network]
        public-network = "{{ .Values.publicNet }}"

    type: Opaque
type: addons.cluster.x-k8s.io/resource-set
---
apiVersion: v1
data:
  data: |
    apiVersion: storage.k8s.io/v1
    kind: CSIDriver
    metadata:
      name: csi.vsphere.vmware.com
    spec:
      attachRequired: true
    ---
    kind: StorageClass
    apiVersion: storage.k8s.io/v1
    metadata:
      name: csi-vsphere-default
      annotations:
        storageclass.kubernetes.io/is-default-class: "true"
    provisioner: csi.vsphere.vmware.com
    parameters:
    # storagepolicyname: "vSAN Default Storage Policy"  #Optional Parameter
    # datastoreurl: "ds:///vmfs/volumes/vsan:52cdfa80721ff516-ea1e993113acfc77/" #Optional Parameter
    # csi.storage.k8s.io/fstype: "ext4" #Optional Parameter
kind: ConfigMap
metadata:
  name: {{ include "cluster.fullname" . }}-csi.vsphere.vmware.com
  namespace: {{ .Release.Namespace }}
---
apiVersion: v1
data:
  data: |
    apiVersion: apps/v1
    kind: DaemonSet
    metadata:
      name: vsphere-csi-node
      namespace: kube-system
    spec:
      selector:
        matchLabels:
          app: vsphere-csi-node
      template:
        metadata:
          labels:
            app: vsphere-csi-node
            role: vsphere-csi
        spec:
          containers:
          - args:
            - --v=5
            - --csi-address=$(ADDRESS)
            - --kubelet-registration-path=$(DRIVER_REG_SOCK_PATH)
            env:
            - name: ADDRESS
              value: /csi/csi.sock
            - name: DRIVER_REG_SOCK_PATH
              value: /var/lib/kubelet/plugins/csi.vsphere.vmware.com/csi.sock
            image: quay.io/k8scsi/csi-node-driver-registrar:v2.0.1
            lifecycle:
              preStop:
                exec:
                  command:
                  - /bin/sh
                  - -c
                  - rm -rf /registration/csi.vsphere.vmware.com-reg.sock /csi/csi.sock
            name: node-driver-registrar
            resources: {}
            securityContext:
              privileged: true
            volumeMounts:
            - mountPath: /csi
              name: plugin-dir
            - mountPath: /registration
              name: registration-dir
          - env:
            - name: CSI_ENDPOINT
              value: unix:///csi/csi.sock
            - name: X_CSI_MODE
              value: node
            - name: X_CSI_SPEC_REQ_VALIDATION
              value: "false"
            - name: VSPHERE_CSI_CONFIG
              value: /etc/cloud/csi-vsphere.conf
            - name: LOGGER_LEVEL
              value: PRODUCTION
            - name: X_CSI_LOG_LEVEL
              value: INFO
            - name: NODE_NAME
              valueFrom:
                fieldRef:
                  fieldPath: spec.nodeName
            image: gcr.io/cloud-provider-vsphere/csi/release/driver:v2.1.0
            livenessProbe:
              failureThreshold: 3
              httpGet:
                path: /healthz
                port: healthz
              initialDelaySeconds: 10
              periodSeconds: 5
              timeoutSeconds: 3
            name: vsphere-csi-node
            ports:
            - containerPort: 9808
              name: healthz
              protocol: TCP
            resources: {}
            securityContext:
              allowPrivilegeEscalation: true
              capabilities:
                add:
                - SYS_ADMIN
              privileged: true
            volumeMounts:
            - mountPath: /etc/cloud
              name: vsphere-config-volume
            - mountPath: /csi
              name: plugin-dir
            - mountPath: /var/lib/kubelet
              mountPropagation: Bidirectional
              name: pods-mount-dir
            - mountPath: /dev
              name: device-dir
          - args:
            - --csi-address=/csi/csi.sock
            image: quay.io/k8scsi/livenessprobe:v2.1.0
            name: liveness-probe
            resources: {}
            volumeMounts:
            - mountPath: /csi
              name: plugin-dir
          dnsPolicy: Default
          tolerations:
          - effect: NoSchedule
            operator: Exists
          - effect: NoExecute
            operator: Exists
          volumes:
          - name: vsphere-config-volume
            secret:
              secretName: csi-vsphere-config
          - hostPath:
              path: /var/lib/kubelet/plugins_registry
              type: Directory
            name: registration-dir
          - hostPath:
              path: /var/lib/kubelet/plugins/csi.vsphere.vmware.com/
              type: DirectoryOrCreate
            name: plugin-dir
          - hostPath:
              path: /var/lib/kubelet
              type: Directory
            name: pods-mount-dir
          - hostPath:
              path: /dev
            name: device-dir
      updateStrategy:
        type: RollingUpdate
kind: ConfigMap
metadata:
  name: {{ include "cluster.fullname" . }}-vsphere-csi-node
  namespace: {{ .Release.Namespace }}
---
apiVersion: v1
data:
  data: |
    apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: vsphere-csi-controller
      namespace: kube-system
    spec:
      replicas: 1
      selector:
        matchLabels:
          app: vsphere-csi-controller
      template:
        metadata:
          labels:
            app: vsphere-csi-controller
            role: vsphere-csi
        spec:
          containers:
          - args:
            - --v=4
            - --timeout=300s
            - --csi-address=$(ADDRESS)
            - --leader-election
            env:
            - name: ADDRESS
              value: /csi/csi.sock
            image: quay.io/k8scsi/csi-attacher:v3.0.0
            name: csi-attacher
            resources: {}
            volumeMounts:
            - mountPath: /csi
              name: socket-dir
          - env:
            - name: CSI_ENDPOINT
              value: unix:///var/lib/csi/sockets/pluginproxy/csi.sock
            - name: X_CSI_MODE
              value: controller
            - name: VSPHERE_CSI_CONFIG
              value: /etc/cloud/csi-vsphere.conf
            - name: LOGGER_LEVEL
              value: PRODUCTION
            - name: X_CSI_LOG_LEVEL
              value: INFO
            image: gcr.io/cloud-provider-vsphere/csi/release/driver:v2.1.0
            livenessProbe:
              failureThreshold: 3
              httpGet:
                path: /healthz
                port: healthz
              initialDelaySeconds: 10
              periodSeconds: 5
              timeoutSeconds: 3
            name: vsphere-csi-controller
            ports:
            - containerPort: 9808
              name: healthz
              protocol: TCP
            resources: {}
            volumeMounts:
            - mountPath: /etc/cloud
              name: vsphere-config-volume
              readOnly: true
            - mountPath: /var/lib/csi/sockets/pluginproxy/
              name: socket-dir
          - args:
            - --csi-address=$(ADDRESS)
            env:
            - name: ADDRESS
              value: /var/lib/csi/sockets/pluginproxy/csi.sock
            image: quay.io/k8scsi/livenessprobe:v2.1.0
            name: liveness-probe
            resources: {}
            volumeMounts:
            - mountPath: /var/lib/csi/sockets/pluginproxy/
              name: socket-dir
          - args:
            - --leader-election
            env:
            - name: X_CSI_FULL_SYNC_INTERVAL_MINUTES
              value: "30"
            - name: LOGGER_LEVEL
              value: PRODUCTION
            - name: VSPHERE_CSI_CONFIG
              value: /etc/cloud/csi-vsphere.conf
            image: gcr.io/cloud-provider-vsphere/csi/release/syncer:v2.1.0
            name: vsphere-syncer
            resources: {}
            volumeMounts:
            - mountPath: /etc/cloud
              name: vsphere-config-volume
              readOnly: true
          - args:
            - --v=4
            - --timeout=300s
            - --csi-address=$(ADDRESS)
            - --leader-election
            - --default-fstype=ext4
            env:
            - name: ADDRESS
              value: /csi/csi.sock
            image: quay.io/k8scsi/csi-provisioner:v2.0.0
            name: csi-provisioner
            resources: {}
            volumeMounts:
            - mountPath: /csi
              name: socket-dir
          dnsPolicy: Default
          serviceAccountName: vsphere-csi-controller
          tolerations:
          - effect: NoSchedule
            key: node-role.kubernetes.io/master
            operator: Exists
          - effect: NoSchedule
            key: node-role.kubernetes.io/control-plane
            operator: Exists
          volumes:
          - name: vsphere-config-volume
            secret:
              secretName: csi-vsphere-config
          - emptyDir: {}
            name: socket-dir
kind: ConfigMap
metadata:
  name: {{ include "cluster.fullname" . }}-vsphere-csi-controller
  namespace: {{ .Release.Namespace }}
---
apiVersion: v1
kind: Secret
metadata:
  name: {{ include "cluster.fullname" . }}-cloud-controller-manager
  namespace: {{ .Release.Namespace }}
stringData:
  data: |
    apiVersion: v1
    kind: ServiceAccount
    metadata:
      labels:
        component: cloud-controller-manager
        vsphere-cpi-infra: service-account
      name: cloud-controller-manager
      namespace: kube-system
type: addons.cluster.x-k8s.io/resource-set
---
apiVersion: v1
kind: Secret
metadata:
  name: {{ include "cluster.fullname" . }}-cloud-provider-vsphere-credentials
  namespace: {{ .Release.Namespace }}
stringData:
  data: |
    apiVersion: v1
    kind: Secret
    metadata:
      labels:
        component: cloud-controller-manager
        vsphere-cpi-infra: secret
      name: cloud-provider-vsphere-credentials
      namespace: kube-system
    stringData:
      {{ .Values.vsphere.host }}.password: {{ .Values.vsphere.password }}
      {{ .Values.vsphere.host }}.username: {{ .Values.vsphere.username }}
    type: Opaque
type: addons.cluster.x-k8s.io/resource-set
---
apiVersion: v1
data:
  data: |
    ---
    apiVersion: rbac.authorization.k8s.io/v1
    kind: ClusterRole
    metadata:
      labels:
        component: cloud-controller-manager
        vsphere-cpi-infra: role
      name: system:cloud-controller-manager
    rules:
    - apiGroups:
      - ""
      resources:
      - events
      verbs:
      - create
      - patch
      - update
    - apiGroups:
      - ""
      resources:
      - nodes
      verbs:
      - '*'
    - apiGroups:
      - ""
      resources:
      - nodes/status
      verbs:
      - patch
    - apiGroups:
      - ""
      resources:
      - services
      verbs:
      - list
      - patch
      - update
      - watch
    - apiGroups:
      - ""
      resources:
      - services/status
      verbs:
      - patch
    - apiGroups:
      - ""
      resources:
      - serviceaccounts
      verbs:
      - create
      - get
      - list
      - watch
      - update
    - apiGroups:
      - ""
      resources:
      - persistentvolumes
      verbs:
      - get
      - list
      - watch
      - update
    - apiGroups:
      - ""
      resources:
      - endpoints
      verbs:
      - create
      - get
      - list
      - watch
      - update
    - apiGroups:
      - ""
      resources:
      - secrets
      verbs:
      - get
      - list
      - watch
    - apiGroups:
      - coordination.k8s.io
      resources:
      - leases
      verbs:
      - get
      - watch
      - list
      - update
      - create
    ---
    apiVersion: rbac.authorization.k8s.io/v1
    kind: ClusterRoleBinding
    metadata:
      labels:
        component: cloud-controller-manager
        vsphere-cpi-infra: cluster-role-binding
      name: system:cloud-controller-manager
    roleRef:
      apiGroup: rbac.authorization.k8s.io
      kind: ClusterRole
      name: system:cloud-controller-manager
    subjects:
    - kind: ServiceAccount
      name: cloud-controller-manager
      namespace: kube-system
    - kind: User
      name: cloud-controller-manager
    ---
    apiVersion: v1
    data:
      vsphere.conf: |
        global:
          port: 443
          secretName: cloud-provider-vsphere-credentials
          secretNamespace: kube-system
          thumbprint: '{{ .Values.vsphere.thumbprint }}'
        vcenter:
          {{ .Values.vsphere.host }}:
            datacenters:
            {{- range .Values.vsphere.datacenters }}
              - {{ . | squote }}
            {{- end }}
            server: '{{ .Values.vsphere.host }}'
    kind: ConfigMap
    metadata:
      name: vsphere-cloud-config
      namespace: kube-system
    ---
    apiVersion: rbac.authorization.k8s.io/v1
    kind: RoleBinding
    metadata:
      labels:
        component: cloud-controller-manager
        vsphere-cpi-infra: role-binding
      name: servicecatalog.k8s.io:apiserver-authentication-reader
      namespace: kube-system
    roleRef:
      apiGroup: rbac.authorization.k8s.io
      kind: Role
      name: extension-apiserver-authentication-reader
    subjects:
    - kind: ServiceAccount
      name: cloud-controller-manager
      namespace: kube-system
    - kind: User
      name: cloud-controller-manager
    ---
    apiVersion: apps/v1
    kind: DaemonSet
    metadata:
      labels:
        component: cloud-controller-manager
        tier: control-plane
      name: vsphere-cloud-controller-manager
      namespace: kube-system
    spec:
      selector:
        matchLabels:
          name: vsphere-cloud-controller-manager
      template:
        metadata:
          labels:
            component: cloud-controller-manager
            name: vsphere-cloud-controller-manager
            tier: control-plane
        spec:
          affinity:
            nodeAffinity:
              requiredDuringSchedulingIgnoredDuringExecution:
                nodeSelectorTerms:
                - matchExpressions:
                  - key: node-role.kubernetes.io/control-plane
                    operator: Exists
                - matchExpressions:
                  - key: node-role.kubernetes.io/master
                    operator: Exists
          containers:
          - args:
            - --v=2
            - --cloud-provider=vsphere
            - --cloud-config=/etc/cloud/vsphere.conf
            image: gcr.io/cloud-provider-vsphere/cpi/release/manager:v1.25.0
            name: vsphere-cloud-controller-manager
            resources:
              requests:
                cpu: 200m
            volumeMounts:
            - mountPath: /etc/cloud
              name: vsphere-config-volume
              readOnly: true
          hostNetwork: true
          priorityClassName: system-node-critical
          securityContext:
            runAsUser: 1001
          serviceAccountName: cloud-controller-manager
          tolerations:
          - effect: NoSchedule
            key: node.cloudprovider.kubernetes.io/uninitialized
            value: "true"
          - effect: NoSchedule
            key: node-role.kubernetes.io/master
            operator: Exists
          - effect: NoSchedule
            key: node-role.kubernetes.io/control-plane
            operator: Exists
          - effect: NoSchedule
            key: node.kubernetes.io/not-ready
            operator: Exists
          volumes:
          - configMap:
              name: vsphere-cloud-config
            name: vsphere-config-volume
      updateStrategy:
        type: RollingUpdate
kind: ConfigMap
metadata:
  name: {{ include "cluster.fullname" . }}-cpi-manifests
  namespace: {{ .Release.Namespace }}
